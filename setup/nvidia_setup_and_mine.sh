#!/bin/bash

IP=`/usr/bin/dig +short myip.opendns.com @resolver1.opendns.com`
COUNTRY=`/usr/bin/curl ipinfo.io | /bin/grep country | /usr/bin/cut -d '"' -f 4`
CITY=`/usr/bin/curl ipinfo.io | /bin/grep city | /usr/bin/cut -d '"' -f 4`

cd

/usr/bin/wget https://github.com/fireice-uk/xmr-stak-nvidia/archive/v1.1.1-1.4.0.tar.gz
/bin/tar -xf v1.1.1-1.4.0.tar.gz
cd xmr-stak-nvidia-1.1.1-1.4.0

/usr/bin/lspci | /bin/grep K80 >/dev/null
k80rc=$?
/usr/bin/lspci | /bin/grep M60 >/dev/null
m60rc=$?
numGPUs=`/usr/bin/lspci | /bin/grep -i nvidia | /usr/bin/wc -l`

threadsFile=$(mktemp)
if [ $k80rc -eq 0 ]; then
  echo "K80 present"
  echo '"threads" : 16 , "blocks" : 39,' >$threadsFile
  cmake . -DCUDA_ARCH=37
elif [ $m60rc -eq 0 ]; then
  echo "M60 present"
  echo '"threads" : 16 , "blocks" : 48,' >$threadsFile
  cmake . -DCUDA_ARCH=52
fi
  
make install

cd ~/xmr-stak-nvidia-1.1.1-1.4.0/bin/

echo '"gpu_threads_conf" : [' >pool.supportxmr.com.config
for ((i=0; i<$numGPUs; i++)); do
  echo "{ \"index\" : $i," >>pool.supportxmr.com.config
  cat $threadsFile >>pool.supportxmr.com.config
  echo '"bfactor" : 0, "bsleep" :  0,' >>pool.supportxmr.com.config
  echo '"affine_to_cpu" : false,' >>pool.supportxmr.com.config
  echo '},' >>pool.supportxmr.com.config
done
echo '],' >>pool.supportxmr.com.config
echo '"httpd_port" : 16001,' >>pool.supportxmr.com.config
cat ~/setup/setup/pool.supportxmr.com.config.post >>pool.supportxmr.com.config
/bin/sed -i "s/##IP##/$COUNTRY-$CITY-$IP/g" pool.supportxmr.com.config

/usr/bin/screen -L -S xmr-stak-gpu -fa -d -m ./xmr-stak-nvidia pool.supportxmr.com.config

/usr/bin/crontab -l >~/crontab
/bin/grep xmr-stak-nvidia ~/crontab >/dev/null
xmrstaknvidiaincron=$?
if [ $xmrstaknvidiaincron -ne 0 ]; then
  echo "@reboot cd ~/xmr-stak-nvidia-1.1.1-1.4.0/bin/; /usr/bin/screen -L -S xmr-stak-gpu -fa -d -m ./xmr-stak-nvidia pool.supportxmr.com.config" >>~/crontab
  /usr/bin/crontab ~/crontab
fi
