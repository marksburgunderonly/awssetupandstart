#!/bin/bash


IP=`/usr/bin/dig +short myip.opendns.com @resolver1.opendns.com`
COUNTRY=`/usr/bin/curl ipinfo.io | /bin/grep country | /usr/bin/cut -d '"' -f 4`
CITY=`/usr/bin/curl ipinfo.io | /bin/grep city | /usr/bin/cut -d '"' -f 4`

cd 

/usr/bin/wget https://github.com/fireice-uk/xmr-stak-cpu/archive/v1.3.0-1.5.0.tar.gz
/bin/tar -xf v1.3.0-1.5.0.tar.gz 
cd xmr-stak-cpu-1.3.0-1.5.0/

/usr/bin/cmake .
/usr/bin/make install

cd bin/

echo '"cpu_threads_conf" :' >pool.supportxmr.com.config
echo '[' >>pool.supportxmr.com.config
processors=`cat /proc/cpuinfo | grep processor | cut -d ":" -f 2 | sed -e "s/^ *//" | sed -e "s/ *$//"`
for Number in $processors 
  do
  echo "{ \"low_power_mode\" : false, \"no_prefetch\" : true, \"affine_to_cpu\" : $Number }," >>pool.supportxmr.com.config
done
echo '],' >>pool.supportxmr.com.config
echo '"use_slow_memory" : "warn",' >>pool.supportxmr.com.config
echo '"nicehash_nonce" : false,' >>pool.supportxmr.com.config
echo '"aes_override" : null,' >>pool.supportxmr.com.config
echo '"daemon_mode" : true,' >>pool.supportxmr.com.config
echo '"httpd_port" : 16000,' >>pool.supportxmr.com.config
cat ~/setup/setup/pool.supportxmr.com.config.post >>pool.supportxmr.com.config

/bin/sed -i "s/##IP##/$COUNTRY-$CITY-$IP/g" pool.supportxmr.com.config

/usr/bin/screen -L -S xmr-stak-cpu -fa -d -m ./xmr-stak-cpu pool.supportxmr.com.config

/usr/bin/crontab -l >~/crontab
/bin/grep xmr-stak-cpu ~/crontab >/dev/null
xmrstakcpuincron=$?
if [ $xmrstakcpuincron -ne 0 ]; then
  echo "@reboot cd ~/xmr-stak-cpu-1.3.0-1.5.0/bin/; /usr/bin/screen -L -S xmr-stak-cpu -fa -d -m ./xmr-stak-cpu pool.supportxmr.com.config" >>~/crontab
  /usr/bin/crontab ~/crontab
fi
